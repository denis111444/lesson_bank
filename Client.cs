﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank
{
    class Client
    {
        public string name;
        public string surname;
        public int age;
        public float balance;

        public void ReName(string name)
        {
            this.name = name;
        }

        public void ReSurname(string surname)
        {
            this.surname = surname;
        }

        public void ReAge(int age)
        {
            this.age = age;
        }

        public void ReBalance(float balance)
        {
            this.balance = balance;
        }

        public Client(string name, string surname, int age, float balance)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.balance = balance;
        }




    }
}
