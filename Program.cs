﻿using System;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank("Банк", "ул.Улица");
            bank.AddClient("Иван", "Иванов", 28, 25000);
            bank.AddClient("Петр", "Петров", 36, 12000);
            bank.AddClient("Дмитрий", "Дмитриев", 48, 136000);
            Console.WriteLine(bank.NameBank);
            Console.WriteLine(bank.AdressBank);
            Console.WriteLine("=====================");


            while (true)
            {            
                Console.WriteLine("Выберите операцию банка:");
                Console.WriteLine("1.Добавить клиента");
                Console.WriteLine("2.Удалить клиента");
                Console.WriteLine("3.Изменение информации клиента");
                Console.WriteLine("4.Информация о клиенте");
                Console.WriteLine("5.Информация обо всех клиентах банка");
                int Choice = Convert.ToInt32(Console.ReadLine());              

                if (Choice == 1)
                {
                    Console.WriteLine("Введите имя клиента:");
                    string _name = Console.ReadLine();
                    Console.WriteLine("Введите фамилию клиента:");
                    string _surname = Console.ReadLine();
                    Console.WriteLine("Введите возраст клиента:");
                    int _age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите баланс клиента:");
                    float _balance = Convert.ToInt32(Console.ReadLine());
                    bank.AddClient(_name, _surname, _age, _balance);

                    Console.WriteLine("Клиент успешно создан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 2)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.RemoveClient();

                    Console.WriteLine("Клиент успешно удалён, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 3)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ReClient();

                    Console.WriteLine("Имя успешно изменено, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 4)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ShowClient();

                    Console.WriteLine("Клиент успешно показан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 5)
                {
                    bank.ShowAllClient();

                    Console.WriteLine("Клиенты успешно показаны, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }
            }
        }
    }
}

