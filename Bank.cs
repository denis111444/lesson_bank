﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank
{
    class Bank
    {
        public string NameBank;
        public string AdressBank;
        List<Client> clients = new List<Client>();

        public Bank (string NameBank,string AdressBank)
        {
            this.NameBank = NameBank;
            this.AdressBank = AdressBank;
        }
        public void AddClient(string name, string surname, int age, float balance)
        {
            Client client = new Client(name,surname,age,balance);
            clients.Add(client);
        }
        public void RemoveClient()
        {
            string SearchName = Console.ReadLine();
            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    clients.Remove(clients[i]);
                }
            }
        }

        public void ChangeBalance()
        {
            string SearchName = Console.ReadLine();
            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    float balance = Convert.ToInt32(Console.ReadLine());
                    clients[i].ReBalance(balance);
                }
            }
        }


        public void ShowClient()
        {
            string SearchName = Console.ReadLine();
            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine($"Имя клиента {clients[i].name}");
                    Console.WriteLine($"Фамилия клиента { clients[i].surname}");
                    Console.WriteLine($"Возраст клиента {clients[i].age}");
                    Console.WriteLine($"Баланс клиента {clients[i].balance}");
                    Console.WriteLine("====================");
                }
            }
        }

        public void ShowAllClient()
        {
            clients.Sort((left, right) => left.name.CompareTo(right.name));
            foreach (Client client in clients)
            {
                Console.WriteLine("====================");
                Console.WriteLine("Имя клиента: " + client.name);
                Console.WriteLine("Фамилия клиента: " + client.surname);
                Console.WriteLine("Возраст клиента: " + client.age);
                Console.WriteLine("Баланс клиента: " + client.balance);
                Console.WriteLine("====================");

            }
        }

        public void ReClient()
        {
            string SearchName = Console.ReadLine();

            for (int i = 0; i < clients.Count; i++)
            {
                if (SearchName == clients[i].name)
                {
                    Console.WriteLine("====================");
                    Console.WriteLine($"Имя клиента {clients[i].name}");
                    Console.WriteLine($"Фамилия клиента { clients[i].surname}");
                    Console.WriteLine($"Возраст клиента {clients[i].age}");
                    Console.WriteLine($"Баланс клиента {clients[i].balance}");
                    Console.WriteLine("====================");

                    Console.WriteLine("Выберите изменение:");
                    Console.WriteLine("1.Изменить имя клиента");
                    Console.WriteLine("2.Изменить фамилию клиента");
                    Console.WriteLine("3.Изменить возраст клиента");
                    Console.WriteLine("4.Изменить баланс клиента");


                    int choice = Convert.ToInt32(Console.ReadLine());

                    if (choice == 1)
                    {
                        Console.WriteLine("Введите новое имя");
                        string name = Console.ReadLine();
                        clients[i].ReName(name);
                    }

                    if (choice == 2)
                    {
                        Console.WriteLine("Введите новую фамилию: ");
                        string surname = Console.ReadLine();
                        clients[i].ReSurname(surname);
                    }

                    if (choice == 3)
                    {
                        Console.WriteLine("Введите новый возраст: ");
                        int age = Convert.ToInt32(Console.ReadLine());
                        clients[i].ReAge(age);
                    }

                    if (choice == 4)
                    {
                        Console.WriteLine("Введите новый баланс: ");
                        float balance = Convert.ToInt32(Console.ReadLine());
                        clients[i].ReBalance(balance);
                    }
                }
            }
        }
    }
}
